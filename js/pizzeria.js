window.onload = load();
let pizzas = [];

//DataBase JSON
var dataBase;

function load(){
    let url = "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/pizzeria.json";
    leer_BD(url).then(datos => {
        dataBase = datos;
        document.getElementById('titulo').textContent = dataBase.nombrePizzeria;
    });
}

async function leer_BD(url){
    try {
        let dataBase = await fetch(url);
        let array = await dataBase.json();
        return array;
    } catch (error) {
        alert(error);
    }
}

function crearButton(){
    let validar = document.getElementById("cant_pizzas").value;
    if (validar >= 1) {
        document.getElementById('cant_pizzas').disabled = true;
        document.getElementById('crear_Button').disabled = true;
        document.getElementById('cargar_Button').removeAttribute('hidden');
        let contenedor = document.querySelector('#contenedor_pizzas');
        const pizza = document.querySelector('#pizza_row').content;
        let fragment = document.createDocumentFragment();
        for (let i = 0; i < validar; i++) {
            pizza.querySelector('#text_tamanio').textContent = "Tamaño de la pizza "+(i+1)+": ";
            let clone = pizza.cloneNode(true);
            clone.querySelector('.tamanio_pizza').className += '_'+i;
            fragment.appendChild(clone);

            let sabor1 = "";
            let sabor2 = "";
            let tamanio = "";
            let precioPizza = 0;
            let producto, valor;
            let adicionales1 = [{producto,valor},{producto,valor},{producto,valor},{producto,valor}];
            let adicionales2 = [{producto,valor},{producto,valor},{producto,valor},{producto,valor}];
            let array = {sabor1,sabor2,tamanio,precioPizza,adicionales1,adicionales2};
            pizzas.push(array);
        }
        contenedor.appendChild(fragment);
        document.getElementById('cargarOpciones').hidden = false;
    }else{
        alert("¡Error!");
    }    
}

function cargar_opciones(){
    let tamanio;
    for (let i = 0; i < pizzas.length; i++) {
        tamanio = document.querySelector('.tamanio_pizza_'+i).value;
        pizzas[i].tamanio = tamanio;
    }
    if (typeof(Storage) !== "undefined") {
        // LocalStorage disponible
        localStorage.setItem("pizzas", JSON.stringify(pizzas));
        localStorage.setItem("dataBase", JSON.stringify(dataBase));
    } else {
        // LocalStorage no soportado en este navegador
        alert("LocalStorage no soportado");
    }
}

function load_contenedores(){
    pizzas = JSON.parse(localStorage.getItem("pizzas"));
    dataBase = JSON.parse(localStorage.getItem("dataBase"));
    let contenedor = document.querySelector('#contenedor_opciones');
    const pizza = document.querySelector('#row_pizza').content;
    let fragment = document.createDocumentFragment();
    for (let i = 0; i < pizzas.length; i++) {
        
        let clone = pizza.cloneNode(true);
        clone.querySelector('.row_pizza_div').className += '_'+i;
        clone.querySelector('#numero_pizza').textContent = i+1;
        clone.querySelector('.text_sabor1').className += '_'+i;
        clone.querySelector('.select_1').className += '_'+i;

        let select1 = clone.querySelector('.select_1_'+i).value;
        clone.querySelector('.text_sabor1_'+i).textContent = 'Pizza '+select1;
        clone.querySelector('.select_2').className += '_'+i;

        clone.querySelector('.img_sabor1').className += '_'+i;
        clone.querySelector('.img_sabor1_'+i).src = img_sabor(select1);
        clone.querySelector('.figcaption_1').className += '_'+i;
        clone.querySelector('.figcaption_1_'+i).textContent = select1;

        clone.querySelector('.cont_img_sabor2').className += '_'+i;
        clone.querySelector('.cont_img_sabor2_'+i).hidden = true;
        clone.querySelector('.img_sabor2').className += '_'+i;

        clone.querySelector('.figcaption_2').className += '_'+i;
        
        clone.querySelector('.text_sabor2').className += '_'+i;
        clone.querySelector('.text_sabor2_'+i).textContent = clone.querySelector('.select_2_'+i).value;

        clone.querySelector('.checks_1').className += '_'+i;
        clone.querySelector('.checks_2').className += '_'+i;
        
        fragment.appendChild(clone);
    }
    contenedor.appendChild(fragment);
}

function img_sabor(sabor){
    switch(sabor){
        case 'Napolitana': return dataBase.pizzas[0].url_Imagen;
        case 'Mexicana':return dataBase.pizzas[1].url_Imagen;
        case 'Hawayana':return dataBase.pizzas[2].url_Imagen;
        case 'Vegetariana': return dataBase.pizzas[3].url_Imagen;
        default: break;
    }
}

function selected_1(){
    let select1;
    for (let i = 0; i < pizzas.length; i++) {
        select1 = document.querySelector('.select_1_'+i).value;
        document.querySelector('.text_sabor1_'+i).textContent = 'Pizza '+select1;  
        document.querySelector('.img_sabor1_'+i).src = img_sabor(select1);
    }
}

function selected_2(){
    let select2;
    for (let i = 0; i < pizzas.length; i++) {
        select2 = document.querySelector('.select_2_'+i).value;
        if(select2 !== 'Ninguno'){
            document.querySelector('.text_sabor2_'+i).textContent = 'Pizza '+select2;
            let checks = document.querySelector('.checks_2_'+i).children;
            for (let i = 0; i < checks.length; i++) {
                checks[i].firstElementChild.disabled = false;
            }
            document.querySelector('.cont_img_sabor2_'+i).hidden = false;
            document.querySelector('.img_sabor2_'+i).src = img_sabor(select2);
            document.querySelector('.figcaption_2_'+i).textContent = select2;
        }else{
            document.querySelector('.text_sabor2_'+i).textContent = select2;
            let checks = document.querySelector('.checks_2_'+i).children;
            let check;
            for (let i = 0; i < checks.length; i++) {
                check = checks[i].firstElementChild;
                if(check.checked == true){check.checked = false;}
                check.disabled = true;
            }
            document.querySelector('.cont_img_sabor2_'+i).hidden = true;
        }        
    }

}

function calcular_factura(){
    
    let select1, select2;
    for (let i = 0; i < pizzas.length; i++) {
        select1 = document.querySelector('.select_1_'+i).value;
        select2 = document.querySelector('.select_2_'+i).value;
        
        pizzas[i].sabor1 = select1;
        pizzas[i].sabor2 = select2;
        let valor1 = valorPizza(select1,pizzas[i].tamanio);
        if(select2 != 'Ninguno'){            
            let valor2 = valorPizza(select2,pizzas[i].tamanio);
            if (valor1>valor2) {
                pizzas[i].precioPizza = valor1;
            }else{
                pizzas[i].precioPizza = valor2;
            }
        }else{
            pizzas[i].precioPizza = valor1;
        }
        
        let check;
        let checks = document.querySelector('.checks_1_'+i).children;
        for (let j = 0; j < checks.length; j++) {
            check = checks[j].firstElementChild;
            if(check.checked === true){
                let adicion = checks[j].lastElementChild.textContent;
                pizzas[i].adicionales1[j].producto = adicion;
                pizzas[i].adicionales1[j].valor = valorAdicionales(adicion);
            }else{
                pizzas[i].adicionales1[j].producto = 'Vacio';
                pizzas[i].adicionales1[j].valor = 0;
            }
        }
        checks = document.querySelector('.checks_2_'+i).children;
        if(select2!='Ninguno'){
            for (let j = 0; j < checks.length; j++) {
                check = checks[j].firstElementChild;
                if(check.checked === true){
                    let adicion = checks[j].lastElementChild.textContent;
                    pizzas[i].adicionales2[j].producto = adicion;
                    pizzas[i].adicionales2[j].valor = valorAdicionales(adicion);
                }else{
                    pizzas[i].adicionales2[j].producto = 'Vacio';
                    pizzas[i].adicionales2[j].valor = 0;
                }
            }
        }
    }
    localStorage.removeItem('dataBase');
    localStorage.setItem('pizzas',JSON.stringify(pizzas));
}

function valorPizza(sabor,tamanio){
    let valorTotal = 0;
    if(sabor == 'Ninguno'){return valorTotal;}
    for (let i = 0; i < dataBase.pizzas.length; i++) {
        if(sabor === dataBase.pizzas[i].sabor){
            switch(tamanio){
                case 'Grande': 
                    valorTotal += dataBase.pizzas[i].precio[0].precio;
                    break;
                case 'Mediano': 
                    valorTotal += dataBase.pizzas[i].precio[1].precio;
                    break;   
                case 'Pequeño': 
                    valorTotal += dataBase.pizzas[i].precio[2].precio;
                    break;
            }    
        }
    }
    return valorTotal;
}

function valorAdicionales(adicional){
    switch (adicional) {
        case 'Tocineta': return dataBase.adicional[0].valor;
        case 'Salami': return dataBase.adicional[1].valor;
        case 'Oregano': return dataBase.adicional[2].valor;
        case 'Salchicha': return dataBase.adicional[3].valor;
        default: return 0;
    }
}

function load_Factura() {
    if(pizzas.length == 0){
        pizzas = JSON.parse(localStorage.getItem('pizzas'));
    }

    google.charts.load('current', { 'packages': ['table'] });
    google.charts.setOnLoadCallback(drawTable);

    function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Descripción');
        data.addColumn('string', 'Valor');
        let valorTotal = 0;
        for (let i = 0; i < pizzas.length; i++) {
            let pizza = 'Pizza ' + pizzas[i].tamanio + ' ';
            if (pizzas[i].sabor2 !== 'Ninguno') {
                pizza += 'Mitad ' + pizzas[i].sabor1 + ' y Mitad ' + pizzas[i].sabor2;
            } else {
                pizza += pizzas[i].sabor1;
            }
            let valor = pizzas[i].precioPizza;
            valorTotal += valor;
            data.addRows([
                [pizza, '$'+valor]
            ]);
            let adicionales1 = pizzas[i].adicionales1;
            let adicion;
            for (let j = 0; j < adicionales1.length; j++) {
                adicion = 'Adicional-' + pizzas[i].sabor1 + '-';
                let producto = adicionales1[j].producto;
                valor = adicionales1[j].valor;
                if (producto != 'Vacio') {
                    adicion += producto;
                    valorTotal += valor;
                    data.addRows([
                        [adicion, '$'+valor]
                    ]);
                }
            }
            if (pizzas[i].sabor2 != 'Ninguno') {
                let adicionales2 = pizzas[i].adicionales2;
                let adicion;
                for (let j = 0; j < adicionales2.length; j++) {
                    adicion = 'Adicional-' + pizzas[i].sabor2 + '-';
                    let producto = adicionales2[j].producto;
                    valor = adicionales2[j].valor;
                    if (producto != 'Vacio') {
                        adicion += producto;
                        valorTotal += valor;
                        data.addRows([
                            [adicion, '$'+valor]
                        ]);
                    }
                }
            }
        }
        data.addRows([
            ['Total:', '$'+valorTotal]
        ]);
        var table = new google.visualization.Table(document.getElementById('table_div'));
        table.draw(data, { showRowNumber: false, width: '100%', height: '40%'});
    }
}